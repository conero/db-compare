# db-compare

> 数据比对工具
>
> 基于python









### 设计

*初步设计为命令行工具。*

*通过 SQL 分析或者数据库生成对应的数据库模型。*



*基本设计命令：*

```powershell
# 启动程序
python main.py

# 可自定义模型名称
$ create <model-name>
$ use <model-name>
# 对象调用
(<model-name>) $ query
```





*模型对应名称目录*

```
- cache
	- <model-name>
		- connent 数据库连接信息
		- models
			- <tables>   数据表信息 
		- sqls
			- tb_<tables>.sql
```





### 分支

- *o/un/pyintaller*   不支持的 *pyinstaller* 的目录结构



```powershell
# 创建虚拟运行环境
python -m venv venv

#-r, --requirement
pip install -r ./requirements.txt
```

