# window 打包分发
echo '*************************************'
echo '*****                         *******'
echo '***** bundleIntoSingleExeFile *******'
echo '*****                         *******'
echo '*************************************'

./venv/Scripts/Activate.ps1

$path = './cache/bundle'
if(-not (Test-Path -Path $path )){
    mkdir $path
}
cd $path


# -w 不显示输出的调试信息
# pyinstaller -i ../../demo.ico -w -F ../../demo.py --add-data config
# 编译
# pyinstaller -i ../../source/demo.ico -F ../../demo.py

pyinstaller -F ../../main.py -i ../../main.ico

cd ../../

#键入退出
"Any key to exit"
Read-Host | Out-Null
Exit
